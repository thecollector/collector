The Collector
==========

A DISCORD bot for Marvel's Contest of Champions
---------------------

*Hello, I'm The Collector. Add me to to your DISCORD server and I will provide external content, moderate, and converse with members*

The Collector is an all-in-one Discord bot packed with features, written in NodeJS and using the Discord.JS third-party API. Here are some of the things he can get from around the web:

 - Images and GIFs
 - YouTube videos
 - RSS/Reddit posts
 - Wikipedia articles
 - Twitter timelines
 - Mobile app links
 - MSN Weather forecast
 - Urban Dictionary terms
 - Google search results
 - So much more!
 
In addition, he has many built-in utility functions:

 - Natural language unit conversion
 - Random number generator
 - Discord user profile
 - Reminders via PM
 - Year countdown
 - Stats for members, games, and commands
 - In-chat polls
 
The Collector is fully configurable via private message or online, and can be controlled independently between servers. New servers can be added with Discord OAuth, and admins are automatically detected.

About
-----
The Collector is a fork of the already-running [`@AwesomeBot`](http://add.awesomebot.xyz/) instance. You are free to use this code as a base for your own bot, so long as you abide by the terms set in the license and [apply for a bot hosting token](http://self-host.awesomebot.xyz/). Also, please join our [Discord server](http://discord.awesomebot.xyz/) if you are modifying the code in any way.

[Head to our wiki for more information »](http://wiki.awesomebot.xyz/)

Contribute
----------

Please feel free to make pull requests or open an issue on this repository. Any help is appreciated!
